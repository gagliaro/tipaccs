# this code have been translate from the matlab code of julien brondex used in Brondex, 2019
# it take the data from the inversion to compute the parameter of
# different friction


import glob
import numpy as np
import pyvista as pv
import pandas as pd

# choose law used
# ex: law=[0,1] for 0 weertman non linear and 1 for schoof
law = []

# load data
files = glob.glob('/home/urrutyb/Documents/PhD_TiPACCS/real_vtu/run1_*.pvtu')
files = sorted(files)
blocks = pv.MultiBlock([pv.read(f) for f in files])
for i, block in enumerate(blocks):
    block["node_value"] = np.full(block.n_points, i)
combined = blocks.combine()
print(blocks)

m = 1 / 3  # Exposant loi Weertman NL ou Schoof
Cmax = 0.4  # Cmax loi Schoof
LineWidth = 1.3

g = 9.81
rho_ice = 910
rho_sea = 1028
alpha = blocks[1].point_arrays['alpha']
z_b = blocks[1].point_arrays['zb']
bedrock = blocks[1].point_arrays['bedrock']
h = blocks[1].point_arrays['h']
U = blocks[1].point_arrays['ssavelocity']
X = blocks[1].points

x = X[:, 0]
y = X[:, 1]
u = U[:, 0]
v = U[:, 1]
GM = blocks[1].point_arrays['groundedmask']

beta = 10**alpha


# calcul de la pression effective
Neff = np.zeros(len(z_b))
for i in range(len(z_b)):
    if z_b[i] < 0:
        Neff[i] = (g * h[i] * rho_ice) + (z_b[i] * rho_sea * g)
    else:
        Neff[i] = g * h[i] * rho_ice


Neff[GM == -1] = 0

# Weertman linear
# calul du tau_b initiale
taub = beta * np.sqrt(u**2 + v**2)
taub[GM == -1] = 0

if 0 in law:

    # POUR LA LOI DE WEERTMAN NON LINEAIRE

    # Inversion du Beta_wnl

    Beta_wnl = taub / (np.sqrt(u**2 + v**2))**m
    Beta_wnl = {'X': x, 'Y': y, 'Beta_wnl': Beta_wnl)
    Beta_wnl = pd.DataFrame(Beta_wnl, columns=['X', 'Y', 'Beta_wnl'])
    Beta_wnl.to_csv('Beta_wnl.dat', sep='\t', encoding='ascii')


if 1 in law:

    # POUR LA LOI DE SCHOOF

    taub_inv = taub / Neff
    index = (taub_inv == float('Inf'))

    # taub_surN_XY_SV
    x = np.delete(x, index)
    y = np.delete(y, index)
    u = np.delete(u, index)
    v = np.delete(v, index)
    GM = np.delete(GM, index)
    Neff = np.delete(Neff, index)
    taub = np.delete(taub, index)

    edges = np.arange(0, 10.1, 0.1)

    # figure
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.hist(taub, edges)
    plt.show()

    N_node = np.arange(0, len(x))

    plt.figure(2)
    plt.scatter(N_node, taub / Neff)
    plt.xlabel('noeuds')
    plt.ylabel('tau_{b}/N_{eff}')
    plt.show()

    plt.figure(3)
    plt.scatter(N_node, taub / (Neff * Cmax))
    #plt.plot([0, max(N_node)],[0.4, 0.4],'r')
    plt.xlabel('noeuds')
    plt.ylabel('tau_{b}/N_{eff}*Cmax')
    plt.title(
        'Pour valeur de C_{max} prescrite dans le préambule du fichier matlab')
    plt.show()

    plt.figure(4)
    plt.hist(taub / (Neff * Cmax), edges)
    #plt.plot([0, max(N_node)],[1, 1],'r')
    plt.title(
        'Pour valeur de C_{max} prescrite dans le préambule du fichier matlab')
    plt.show()

    Cmax_tmp = np.arange(0, 10.1, 0.1)
    Nb_knots_SV = np.zeros(len(Cmax_tmp))
    tmp = np.zeros(len(Cmax_tmp))

    for i in range(1, len(Cmax_tmp)):
        tmp = taub / Neff > Cmax_tmp[i]
        Nb_knots_SV[i] = np.sum(tmp)

    fig = plt.figure(5)
    plt.plot(Cmax_tmp, (Nb_knots_SV / len(x)) * 100)
    plt.show()

    # Localisation des points où Cs sera interpollé

    Clim_eff = 0.8

    # Taub_surN_XY_SV=[Taub_surN_XY_SV,
    # logical(Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4)>Cmax)];

    logical = (taub / Neff) > Clim_eff * Cmax

    # taub_surN_XY_IkenBound_SV
    x = np.delete(x, index)
    y = np.delete(y, index)
    u = np.delete(u, index)
    v = np.delete(v, index)
    GM = np.delete(GM, index)
    Neff = np.delete(Neff, index)
    taub = np.delete(taub, index)

    # Calcul du Cs pour différent Cmax et Clim_eff

    Clim_eff_tmp = np.arange(0.4, 1.01, 0.01)
    Cmax04 = 0.4
    Cmax06 = 0.6

    # Calcul du pourcentage de noeud supprimé pour les 2 cas de Cmax

    Nombre_noeuds_Cmax04_SV = np.zeros(len(Clim_eff_tmp))
    for i in range(len(Clim_eff_tmp)):
        i
        tmp = np.multiply(((taub / Neff)) > Clim_eff_tmp[i] * Cmax04, 1)
        Nombre_noeuds_Cmax04_SV[i] = np.sum(tmp)

    Nombre_noeuds_Cmax06_SV = np.zeros(len(Clim_eff_tmp))
    for i in range(len(Clim_eff_tmp)):
        i
        tmp = np.multiply(((taub / Neff)) > Clim_eff_tmp[i] * Cmax06, 1)
        Nombre_noeuds_Cmax06_SV[i] = np.sum(tmp)

    plt. figure(10)
    l1 = plt.plot(
        Clim_eff_tmp,
        (Nombre_noeuds_Cmax04_SV / len(taub)) * 100,
        'g')
    l2 = plt.plot(
        Clim_eff_tmp,
        (Nombre_noeuds_Cmax06_SV / len(taub)) * 100,
        'b')
    plt. title(
        'pourcentage des noeuds non-flottants pour lesquels le C_S sera interpollé')
    plt.show()

    # ~~~~~~~~~~~~~ Csmax et Csmoy pour les 2 Cmax en fonction de Clim ~~~~~~~

    # 1°/ Il faut retirer tous les noeuds qui ne respecte pas la borne d'Iken

    index = (taub / Neff) > Clim_eff * Cmax04

    # taub_surN_XY_IkenBound_SV
    x04 = np.delete(x, index)
    y04 = np.delete(y, index)
    u04 = np.delete(u, index)
    v04 = np.delete(v, index)
    GM04 = np.delete(GM, index)
    Neff04 = np.delete(Neff, index)
    taub04 = np.delete(taub, index)

    index = (taub / Neff) > Clim_eff * Cmax06

    # taub_surN_XY_IkenBound_SV
    x06 = np.delete(x, index)
    y06 = np.delete(y, index)
    u06 = np.delete(u, index)
    v06 = np.delete(v, index)
    GM06 = np.delete(GM, index)
    Neff06 = np.delete(Neff, index)
    taub06 = np.delete(taub, index)

    # 2°/Calcul du Cs
    # ~~~Cmax=0.4
    ub_Cmax04_SV = np.sqrt(u04**2 + v04**2)
    Cs_Cmax04_SV = taub04 / \
        (ub_Cmax04_SV**m * (1 - (taub04 / Neff04)**(1 / m))**m)

    Cs_Cmax04_moy_SV = np.zeros(len(Clim_eff_tmp))
    Cs_Cmax04_min_SV = np.zeros(len(Clim_eff_tmp))
    Cs_Cmax04_max_SV = np.zeros(len(Clim_eff_tmp))

    for i in range(len(Clim_eff_tmp)):
        tmp = ((taub04 / Neff04) > Clim_eff_tmp[i] * Cmax04)
        tmp = np.delete(Cs_Cmax04_SV, tmp)
        Cs_Cmax04_min_tmp = min(tmp)
        Cs_Cmax04_max_tmp = max(tmp)
        Cs_Cmax04_moy_tmp = np.mean(tmp)
        Cs_Cmax04_min_SV[i] = Cs_Cmax04_min_tmp
        Cs_Cmax04_max_SV[i] = Cs_Cmax04_max_tmp
        Cs_Cmax04_moy_SV[i] = Cs_Cmax04_moy_tmp

    # ~~~Cmax=0.6
    ub_Cmax06_SV = np.sqrt(u06**2 + v06**2)
    Cs_Cmax06_SV = taub06 / \
        (ub_Cmax06_SV**m * (1 - (taub06 / Neff06)**(1 / m))**m)

    Cs_Cmax06_moy_SV = np.zeros(len(Clim_eff_tmp))
    Cs_Cmax06_min_SV = np.zeros(len(Clim_eff_tmp))
    Cs_Cmax06_max_SV = np.zeros(len(Clim_eff_tmp))

    for i in range(len(Clim_eff_tmp)):
        tmp = ((taub06 / Neff06) > Clim_eff_tmp[i] * Cmax06)
        tmp = np.delete(Cs_Cmax06_SV, tmp)
        Cs_Cmax06_min_tmp = min(tmp)
        Cs_Cmax06_max_tmp = max(tmp)
        Cs_Cmax06_moy_tmp = np.mean(tmp)
        Cs_Cmax06_min_SV[i] = Cs_Cmax06_min_tmp
        Cs_Cmax06_max_SV[i] = Cs_Cmax06_max_tmp
        Cs_Cmax06_moy_SV[i] = Cs_Cmax06_moy_tmp

    # ~~~~~~~~~~~ON TRACE LES INFOS~~~~~~~~~~~~~~~~~~~~~~~~~
    # fig4 = figure('renderer','painters','units','centimeters','position',[0 0 17.8 13]);hold on;
    # % figure(10); hold on;
    # % l1=plot(Clim_eff_tmp,Cs_Cmax04_min,'g:')
    # l2=plot(Clim_eff_tmp,Cs_Cmax04_moy_SV,'g-')
    # l3=plot(Clim_eff_tmp,Cs_Cmax04_max_SV,'g--')
    # % l4=plot(Clim_eff_tmp,Cs_Cmax06_min,'b:')
    # l5=plot(Clim_eff_tmp,Cs_Cmax06_moy_SV,'b-')
    # l6=plot(Clim_eff_tmp,Cs_Cmax06_max_SV,'b--')
    # hL = legend([l2,l3,l5,l6],{'C_{max}=0.4, C_S moyen','C_{max}=0.4, C_S max','C_{max}=0.6, C_S moyen','C_{max}=0.6, C_S max'})
    # xlabel('valeur C_{lim}')
    # ylabel('Valeur C_S')
    # ax=gca
    # ax.Box = 'On'
    # title('Moyenne et maxi du C_S pour deux C_{max} selon C_{lim}')
    # % xlim([0 2])
    # grid on

    # Etape Final : On choisit un Clim_final et on sort Cs pour tous les noeuds qui respectent tb/N < Clim_final * Cmax avec Cmax = 0.4 et Cmax = 0.6
    # Attention ici on veut garder les noeuds flottants et leur affecté un Cs
    # petit

    Clim_final = 0.8

    # Etape préléminaire : récupérer les infos sur tous les noeuds (même
    # flottant)
    alpha = blocks[1].point_arrays['alpha']
    z_b = blocks[1].point_arrays['zb']
    bedrock = blocks[1].point_arrays['bedrock']
    h = blocks[1].point_arrays['h']
    U = blocks[1].point_arrays['ssavelocity']
    X = blocks[1].points

    x = X[:, 0]
    y = X[:, 1]
    u = U[:, 0]
    v = U[:, 1]
    GM = blocks[1].point_arrays['groundedmask']

    beta = 10**alpha

    # calcul de la pression effective
    Neff = np.zeros(len(z_b))
    for i in range(len(z_b)):
        if z_b[i] < 0:
            Neff[i] = (g * h[i] * rho_ice) + (z_b[i] * rho_sea * g)
        else:
            Neff[i] = g * h * rho_ice

    Neff[GM == -1] = 0

    # calul du tau_b initiale
    taub = beta * np.sqrt(u**2 + v**2)
    taub[GM == -1] = 0

    # Pour Cmax = 04
    # 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont flottants
    # XYTaub_CsFinal_Cmax04_SV=[XYTaub_Allnodes_SV(:,1:7),logical((XYTaub_Allnodes_SV(:,3)./XYTaub_Allnodes_SV(:,4))>Clim_final*Cmax04)];
    # XYTaub_CsFinal_Cmax04_SV(XYTaub_CsFinal_Cmax04_SV(:,7)==0 &
    # XYTaub_CsFinal_Cmax04_SV(:,8)==1,:)=[];
    Nf = (Neff == 0)
    index = ((Neff == 0) & (taub / Neff > Clim_final * Cmax04))

    # XYTaub_CsFinal_Cmax04_SV
    x = np.delete(x, index)
    y = np.delete(y, index)
    u = np.delete(u, index)
    v = np.delete(v, index)
    GM = np.delete(GM, index)
    Neff = np.delete(Neff, index)
    taub = np.delete(taub, index)
    Nf = np.delete(Nf, index)

    # 2- on crée le fichier X Y Cs correspondant à ces noeuds
    ubFinal_Cmax04_tmp_SV = np.sqrt(u**2 + v**2)
    CsFinal_Cmax04_SV = taub / \
        (ubFinal_Cmax04_tmp_SV**m * (1 - (taub / Neff)**(1 / m))**m)

    # CsFinal_Cmax04_tmp_SV =
    # [XYTaub_CsFinal_Cmax04_SV(:,1:2),XYTaub_CsFinal_Cmax04_SV(:,7),CsFinal_Cmax04_tmp_SV];

    # 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique
    # zones posées à écoulement rapide)
    CsFinal_Cmax04_SV[Nf] = 10**-3

    # 4- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV' par 'SansVisco' ou autre directement dans dossier de sauvegarde
    # np.save('Cs_Cmax04_Clim08_PROV.dat','CsFinal_Cmax04','-ASCII')

    # # Pour Cmax = 06

    # # 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont flottants
    # XYTaub_CsFinal_Cmax06_SV=[XYTaub_Allnodes_SV(:,1:7),logical((XYTaub_Allnodes_SV(:,3)./XYTaub_Allnodes_SV(:,4))>Clim_final*Cmax06)];
    # XYTaub_CsFinal_Cmax06_SV(XYTaub_CsFinal_Cmax06_SV(:,7)==0 & XYTaub_CsFinal_Cmax06_SV(:,8)==1,:)=[];
    # XYTaub_CsFinal_Cmax06_SV=XYTaub_CsFinal_Cmax06_SV(:,1:7);

    # # 2- on crée le fichier X Y Cs correspondant à ces noeuds
    # ubFinal_Cmax06_tmp_SV = sqrt(XYTaub_CsFinal_Cmax06_SV(:,5).^2+XYTaub_CsFinal_Cmax06_SV(:,6).^2);
    # CsFinal_Cmax06_tmp_SV =
    # XYTaub_CsFinal_Cmax06_SV(:,3)./(ubFinal_Cmax06_tmp_SV.^m.*(1-(XYTaub_CsFinal_Cmax06_SV(:,3)./(Cmax06.*XYTaub_CsFinal_Cmax06_SV(:,4))).^(1/m)).^m);

    # CsFinal_Cmax06_tmp_SV =
    # [XYTaub_CsFinal_Cmax06_SV(:,1:2),XYTaub_CsFinal_Cmax06_SV(:,7),CsFinal_Cmax06_tmp_SV];

    # # 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique zones posées à écoulement rapide)
    # CsFinal_Cmax06_tmp_SV(CsFinal_Cmax06_tmp_SV(:,3)==1,4) = 10^-3;
    # CsFinal_Cmax06_SV =
    # [CsFinal_Cmax06_tmp_SV(:,1:2),CsFinal_Cmax06_tmp_SV(:,4)];

    # # 4- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV' par 'SansVisco' ou autre directement dans dossier de sauvegarde
    # % save('Cs_Cmax06_Clim08_PROV.dat','CsFinal_Cmax06_SV','-ASCII')
    # +end_src

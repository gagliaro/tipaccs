clear all
close all
format long

orange=[223/255,109/255,20/255];

%%% Couleur pour Plot
orange = [37/255,253/255,233/255];
Blue = [0.22265625, 0.66015625, 0.859375];


DarkBlue = [0/255,76/255,153/255];

Purple = [0.3828125, 0.12890625, 0.50390625]; 

Brown = [0.3828125, 0.12890625, 0.50390625];  %% En vrai violet mais la flemme de modifier dans le code

Tomato = [46/255,139/255,87/255]; 

LightGrey=[218/255,218/255,218/255];

Bl = [0.22265625, 0.66015625, 0.859375]; % 1F

Vio = [0.3828125, 0.12890625, 0.50390625]; % DI

Ma = [0.7890625, 0.6171875, 0.390625]; % Last G

Ro = [205/255,51/255,51/255]; % 'c' ; %[238/255, 44/255, 44/255];

%%%%%%% Nom des variables %%%%%

X = 1;
Y = 2;
GM = 3;
alpha = 4;
Neff = 5;
U = 6;
V = 7;

m=1/3; %Exposant loi Weertman NL ou Schoof
Cmax=0.4;% Cmax loi Schoof
LineWidth=1.3;


%% CHOIX DU CAS : FAIRE TRES ATTENTION

  cd /home/brondexj/Documents/Thèse/AMUNDSEN/sorties_Occigen/Relaxation_WeertmanLine/SansVisco
  XY_Data_SV = load('XY_Data.dat');
 cd /home/brondexj/Documents/Thèse/AMUNDSEN/sorties_Occigen/Relaxation_WeertmanLine/Visco_Ga1/
  XY_Data_Ga1 = load('XY_Data.dat');
 %cd /home/brondexj/Documents/Thèse/AMUNDSEN/sorties_Occigen/Relaxation_WeertmanLine/Visco_Ga10/
  cd /home/brondexj/Documents/Thèse/AMUNDSEN/sorties_Occigen/Relaxation_WeertmanLine/Visco_Ga100/
  XY_Data_Ga100 = load('XY_Data.dat');


%%% On force N à 0 aux noeuds flottants
XY_Data_SV(XY_Data_SV(:,GM)==-1,Neff)=0;
XY_Data_Ga1(XY_Data_Ga1(:,GM)==-1,Neff)=0;
XY_Data_Ga100(XY_Data_Ga100(:,GM)==-1,Neff)=0;

% %%%% Calcul du taub initial (loi de Weertman)

BetaW_SV=10.^XY_Data_SV(:,alpha); %%% Attention, l'inversion est fait avec une loi de Weertman LINEAIRE
XY_Taub_SV=[XY_Data_SV(:,X) XY_Data_SV(:,Y) XY_Data_SV(:,GM) BetaW_SV.*sqrt(XY_Data_SV(:,U).^2+XY_Data_SV(:,V).^2)];
XY_Taub_SV(XY_Taub_SV(:,3)==-1,4)=0; %%% On force le taub à zéro aux noeuds flottants
XY_Taub_SV=[XY_Taub_SV(:,1:2),XY_Taub_SV(:,4)]; %%% Pour avoir (X,Y,TauB)

BetaW_Ga1=10.^XY_Data_Ga1(:,alpha); %%% Attention, l'inversion est fait avec une loi de Weertman LINEAIRE
XY_Taub_Ga1=[XY_Data_Ga1(:,X) XY_Data_Ga1(:,Y) XY_Data_Ga1(:,GM) BetaW_Ga1.*sqrt(XY_Data_Ga1(:,U).^2+XY_Data_Ga1(:,V).^2)];
XY_Taub_Ga1(XY_Taub_Ga1(:,3)==-1,4)=0; %%% On force le taub à zéro aux noeuds flottants
XY_Taub_Ga1=[XY_Taub_Ga1(:,1:2),XY_Taub_Ga1(:,4)]; %%% Pour avoir (X,Y,TauB)

BetaW_Ga100=10.^XY_Data_Ga100(:,alpha); %%% Attention, l'inversion est fait avec une loi de Weertman LINEAIRE
XY_Taub_Ga100=[XY_Data_Ga100(:,X) XY_Data_Ga100(:,Y) XY_Data_Ga100(:,GM) BetaW_Ga100.*sqrt(XY_Data_Ga100(:,U).^2+XY_Data_Ga100(:,V).^2)];
XY_Taub_Ga100(XY_Taub_Ga100(:,3)==-1,4)=0; %%% On force le taub à zéro aux noeuds flottants
XY_Taub_Ga100=[XY_Taub_Ga100(:,1:2),XY_Taub_Ga100(:,4)]; %%% Pour avoir (X,Y,TauB)


% %% POUR LA LOI DE WEERTMAN NON LINEAIRE
% 
% %%% Inversion du Beta_wnl
% 
% Beta_wnl=XY_Taub(:,3)./(sqrt(XY_Data(:,U).^2+XY_Data(:,V).^2)).^m;
% Beta_wnl=[XY_Data(:,X),XY_Data(:,Y),Beta_wnl];
% 
% %%% Sauvegarde du Beta_wnl : ATTENTION A SAUVEGARDER DANS LE BON DOSSIER
% % save('Beta_wnl_SansVisco.dat','Beta_wnl','-ASCII')
% % save('Beta_wnl_ViscoGa1.dat','Beta_wnl','-ASCII')
% % save('Beta_wnl_ViscoGa10.dat','Beta_wnl','-ASCII')
% % save('Beta_wnl_ViscoGa100.dat','Beta_wnl','-ASCII')

%% POUR LA LOI DE SCHOOF
%%%% CHOIX DU Cmax -- Visualisation Taub_sur_N
%%%Taub_surN_XY : (X,Y,TauB,N,U,V)
Taub_surN_XY_SV=[XY_Data_SV(:,X), XY_Data_SV(:,Y), XY_Taub_SV(:,3), XY_Data_SV(:,Neff), XY_Data_SV(:,U), XY_Data_SV(:,V) isnan(XY_Taub_SV(:,3)./XY_Data_SV(:,Neff))];
Taub_surN_XY_SV(Taub_surN_XY_SV(:,7)==1,:)=[]; %%% On retire les NaN
Taub_surN_XY_SV=Taub_surN_XY_SV(:,1:6);

Taub_surN_XY_Ga1=[XY_Data_Ga1(:,X), XY_Data_Ga1(:,Y), XY_Taub_Ga1(:,3), XY_Data_Ga1(:,Neff), XY_Data_Ga1(:,U), XY_Data_Ga1(:,V) isnan(XY_Taub_Ga1(:,3)./XY_Data_Ga1(:,Neff))];
Taub_surN_XY_Ga1(Taub_surN_XY_Ga1(:,7)==1,:)=[]; %%% On retire les NaN
Taub_surN_XY_Ga1=Taub_surN_XY_Ga1(:,1:6);

Taub_surN_XY_Ga100=[XY_Data_Ga100(:,X), XY_Data_Ga100(:,Y), XY_Taub_Ga100(:,3), XY_Data_Ga100(:,Neff), XY_Data_Ga100(:,U), XY_Data_Ga100(:,V) isnan(XY_Taub_Ga100(:,3)./XY_Data_Ga100(:,Neff))];
Taub_surN_XY_Ga100(Taub_surN_XY_Ga100(:,7)==1,:)=[]; %%% On retire les NaN
Taub_surN_XY_Ga100=Taub_surN_XY_Ga100(:,1:6);


edges=[0:0.1:10]';
 
% figure(2);hold on;
% histogram(Taub_surN_XY(:,3),edges)
% xlabel('valeur de tau_b/N')
% ylabel('Nombre de noeuds')


N_node=[1:1:size(Taub_surN_XY_SV,1)]';
% figure(3);hold on;
% scatter(N_node,Taub_surN_XY(:,3)./Taub_surN_XY(:,4),10,'+k')
% xlabel('Numéro du noeud')
% ylabel('Valeur de \tau_b / N')
% 
% ylim([0 20])

% figure(4);hold on;
% scatter(N_node,(Taub_surN_XY(:,3)./(Taub_surN_XY(:,4).*Cmax)),10,'+k')
% plot([0 max(N_node)],[1 1],'r-','Linewidth',2)
% xlabel('Numéro du noeud')
% ylabel('Valeur de \tau_b / C_{max}N')
% title('Pour valeur de C_{max} prescrite dans le préambule du fichier matlab')
% 
% ylim([0 20])

% figure(5);hold on;
% histogram((Taub_surN_XY(:,3)./(Taub_surN_XY(:,4).*Cmax)),edges)
% plot([1 1],[0 max(N_node)],'r-','Linewidth',2)
% xlabel('valeur de tau_b/ C_{max}N')
% ylabel('Nombre de noeuds')
% title('Pour valeur de C_{max} prescrite dans le préambule du fichier matlab')

%%% On trace une courbe qui donne le pourcentage de noeuds tq tb/N>Cmax en
%%% fonction de Cmax

Cmax_tmp=[0:0.01:10]';
Nombre_noeuds_SV=[];
tmp=[];
for i=1:size(Cmax_tmp,1)
    i
    tmp=logical((Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4))>Cmax_tmp(i));
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_SV=[Nombre_noeuds_SV;Nombre_noeuds_tmp];
end

Nombre_noeuds_Ga1=[];
tmp=[];
for i=1:size(Cmax_tmp,1)
    i
    tmp=logical((Taub_surN_XY_Ga1(:,3)./Taub_surN_XY_Ga1(:,4))>Cmax_tmp(i));
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Ga1=[Nombre_noeuds_Ga1;Nombre_noeuds_tmp];
end

Nombre_noeuds_Ga100=[];
tmp=[];
for i=1:size(Cmax_tmp,1)
    i
    tmp=logical((Taub_surN_XY_Ga100(:,3)./Taub_surN_XY_Ga100(:,4))>Cmax_tmp(i));
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Ga100=[Nombre_noeuds_Ga100;Nombre_noeuds_tmp];
end

% fig1 = figure('renderer','painters','units','centimeters','position',[0 0 17.8 13]);hold on;
fig15 = figure('renderer','painters','units','centimeters','position',[0 0 17.8 13]);hold on;
% plot(Cmax_tmp,(Nombre_noeuds_SV./size(Taub_surN_XY_SV,1))*100,'-','Color','k','LineWidth',LineWidth)
plot(Cmax_tmp,(Nombre_noeuds_Ga100./size(Taub_surN_XY_Ga100,1))*100,'-','Color','k','LineWidth',LineWidth)
% plot(Cmax_tmp,(Nombre_noeuds_Ga1./size(Taub_surN_XY_Ga1,1))*100,':','Color',Ma,'LineWidth',LineWidth)
xlabel('C_{max}')
ylabel('Grounded nodes where \tau_b / N > C_{max} (%)')
xlim([0.2 0.8])
grid on 
ax=gca
ax.Box = 'On'
% cd /home/brondexj/Documents/Thèse/Manuscript_these/Mon_Manuscript/chapitre6/Images/
% save2pdf('Figure_IkenBound_Cmax',gcf,600);


%% Localisation des points où Cs sera interpollé  


Clim_eff = 0.8; % Pourcentage du Cmax tq max(Taub/N)=Clim_eff*Cmax

%%% Localisation des noeuds retirés :
%%% Rouge = (Taub/N) > Cmax
%%% Orange = (Taub/N)> Clim_eff * Cmax
%%% Vert = (Taub/N) < Clim_eff * Cmax
%%% Les noeuds flottants (N=0) on déjà été retirés

Taub_surN_XY_SV=[Taub_surN_XY_SV, logical(Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4)>Cmax)];

Taub_surN_XY_IkenBound_SV=Taub_surN_XY_SV;
Taub_surN_XY_IkenBound_SV(Taub_surN_XY_IkenBound_SV(:,7)==1,:)=[]; %% On retire tous les noeuds tq tb/N>Cmax
Taub_surN_XY_IkenBound_SV=[Taub_surN_XY_IkenBound_SV(:,1:6), logical((Taub_surN_XY_IkenBound_SV(:,3)./Taub_surN_XY_IkenBound_SV(:,4))>Clim_eff*Cmax)];

Taub_surN_XY_Ga1=[Taub_surN_XY_Ga1, logical(Taub_surN_XY_Ga1(:,3)./Taub_surN_XY_Ga1(:,4)>Cmax)];

Taub_surN_XY_IkenBound_Ga1=Taub_surN_XY_Ga1;
Taub_surN_XY_IkenBound_Ga1(Taub_surN_XY_IkenBound_Ga1(:,7)==1,:)=[]; %% On retire tous les noeuds tq tb/N>Cmax
Taub_surN_XY_IkenBound_Ga1=[Taub_surN_XY_IkenBound_Ga1(:,1:6), logical((Taub_surN_XY_IkenBound_Ga1(:,3)./Taub_surN_XY_IkenBound_Ga1(:,4))>Clim_eff*Cmax)];

Taub_surN_XY_Ga100=[Taub_surN_XY_Ga100, logical(Taub_surN_XY_Ga100(:,3)./Taub_surN_XY_Ga100(:,4)>Cmax)];

Taub_surN_XY_IkenBound_Ga100=Taub_surN_XY_Ga100;
Taub_surN_XY_IkenBound_Ga100(Taub_surN_XY_IkenBound_Ga100(:,7)==1,:)=[]; %% On retire tous les noeuds tq tb/N>Cmax
Taub_surN_XY_IkenBound_Ga100=[Taub_surN_XY_IkenBound_Ga100(:,1:6), logical((Taub_surN_XY_IkenBound_Ga100(:,3)./Taub_surN_XY_IkenBound_Ga100(:,4))>Clim_eff*Cmax)];

% fig2 = figure('renderer','painters','units','centimeters','position',[0 0 13 13]);hold on;
% h2=scatter(Taub_surN_XY_Ga1(Taub_surN_XY_Ga1(:,7)==0,1),Taub_surN_XY_Ga1(Taub_surN_XY_Ga1(:,7)==0,2),0.05,'b+')
% scatter(XY_Data_Ga1(XY_Data_Ga1(:,GM)==-1,1),XY_Data_Ga1(XY_Data_Ga1(:,GM)==-1,2),0.05,'g+')
% h3=scatter(Taub_surN_XY_IkenBound_Ga1(Taub_surN_XY_IkenBound_Ga1(:,7)==1,1),Taub_surN_XY_IkenBound_Ga1(Taub_surN_XY_IkenBound_Ga1(:,7)==1,2),0.05,'r+')
% 
% ax=gca
% ax.Box= 'On' % Pour avoir les axes sur les 4 côtés
% set(gca,'FontSize',9,'xtick',[],'ytick',[])
% % cd /home/brondexj/Documents/Thèse/Manuscript_these/Mon_Manuscript/chapitre6/Images/
% % save2pdf('Figure_Interpol_Cs',gcf,600);


%% Calcul du Cs pour différent Cmax et Clim_eff

Clim_eff_tmp=[0.4:0.01:1]';
Cmax04=0.4;
Cmax06=0.6;

%%% Calcul du pourcentage de noeud supprimé pour les 2 cas de Cmax

Nombre_noeuds_Cmax04_SV=[];
tmp=[];
for i=1:size(Clim_eff_tmp,1)
    i
    tmp=logical((Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4))>Clim_eff_tmp(i)*Cmax04);
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Cmax04_SV=[Nombre_noeuds_Cmax04_SV;Nombre_noeuds_tmp];
end

Nombre_noeuds_Cmax06_SV=[];
tmp=[];
for i=1:size(Clim_eff_tmp,1)
    i
    tmp=logical((Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4))>Clim_eff_tmp(i)*Cmax06);
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Cmax06_SV=[Nombre_noeuds_Cmax06_SV;Nombre_noeuds_tmp];
end

Nombre_noeuds_Cmax04_Ga1=[];
tmp=[];
for i=1:size(Clim_eff_tmp,1)
    i
    tmp=logical((Taub_surN_XY_Ga1(:,3)./Taub_surN_XY_Ga1(:,4))>Clim_eff_tmp(i)*Cmax04);
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Cmax04_Ga1=[Nombre_noeuds_Cmax04_Ga1;Nombre_noeuds_tmp];
end

Nombre_noeuds_Cmax06_Ga1=[];
tmp=[];
for i=1:size(Clim_eff_tmp,1)
    i
    tmp=logical((Taub_surN_XY_Ga1(:,3)./Taub_surN_XY_Ga1(:,4))>Clim_eff_tmp(i)*Cmax06);
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Cmax06_Ga1=[Nombre_noeuds_Cmax06_Ga1;Nombre_noeuds_tmp];
end

Nombre_noeuds_Cmax04_Ga100=[];
tmp=[];
for i=1:size(Clim_eff_tmp,1)
    i
    tmp=logical((Taub_surN_XY_Ga100(:,3)./Taub_surN_XY_Ga100(:,4))>Clim_eff_tmp(i)*Cmax04);
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Cmax04_Ga100=[Nombre_noeuds_Cmax04_Ga100;Nombre_noeuds_tmp];
end

Nombre_noeuds_Cmax06_Ga100=[];
tmp=[];
for i=1:size(Clim_eff_tmp,1)
    i
    tmp=logical((Taub_surN_XY_Ga100(:,3)./Taub_surN_XY_Ga100(:,4))>Clim_eff_tmp(i)*Cmax06);
    Nombre_noeuds_tmp=sum(tmp);
    Nombre_noeuds_Cmax06_Ga100=[Nombre_noeuds_Cmax06_Ga100;Nombre_noeuds_tmp];
end

% Fig21=figure('renderer','painters','units','centimeters','position',[0 0 17.8 13]);hold on;
figure(10); hold on;
l1=plot(Clim_eff_tmp,(Nombre_noeuds_Cmax04_SV./size(Taub_surN_XY_SV,1))*100,'g-')
l2=plot(Clim_eff_tmp,(Nombre_noeuds_Cmax06_SV./size(Taub_surN_XY_SV,1))*100,'b-')
plot(Clim_eff_tmp,(Nombre_noeuds_Cmax04_Ga100./size(Taub_surN_XY_Ga100,1))*100,'g--')
plot(Clim_eff_tmp,(Nombre_noeuds_Cmax06_Ga100./size(Taub_surN_XY_Ga100,1))*100,'b--')
plot(Clim_eff_tmp,(Nombre_noeuds_Cmax04_Ga1./size(Taub_surN_XY_Ga1,1))*100,'g:')
plot(Clim_eff_tmp,(Nombre_noeuds_Cmax06_Ga1./size(Taub_surN_XY_Ga1,1))*100,'b:')
hL = legend([l1,l2],{'C_{max}=0.4','C_{max}=0.6'})
xlabel('valeur C_{lim}')
ylabel('Pourcentage de noeuds tel que \tau_b/N >C_{lim}*C_{max}')
ax=gca
ax.Box = 'On'
title('pourcentage des noeuds non-flottants pour lesquels le C_S sera interpollé')
% xlim([0 2])
grid on 

%%%~~~~~~~~~~~~~ Csmax et Csmoy pour les 2 Cmax en fonction de Clim ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%%


%%% 1°/ Il faut retirer tous les noeuds qui ne respecte pas la borne d'Iken

Taub_surN_XY_IkenBound_Cmax04_SV=[Taub_surN_XY_SV(:,1:6),logical((Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4))>Cmax04)];
Taub_surN_XY_IkenBound_Cmax04_SV(Taub_surN_XY_IkenBound_Cmax04_SV(:,7)==1,:)=[];
Taub_surN_XY_IkenBound_Cmax04_SV=Taub_surN_XY_IkenBound_Cmax04_SV(:,1:6);

Taub_surN_XY_IkenBound_Cmax06_SV=[Taub_surN_XY_SV(:,1:6),logical((Taub_surN_XY_SV(:,3)./Taub_surN_XY_SV(:,4))>Cmax06)];
Taub_surN_XY_IkenBound_Cmax06_SV(Taub_surN_XY_IkenBound_Cmax06_SV(:,7)==1,:)=[];
Taub_surN_XY_IkenBound_Cmax06_SV=Taub_surN_XY_IkenBound_Cmax06_SV(:,1:6);

%%% 2°/Calcul du Cs
%%%~~~Cmax=0.4
ub_Cmax04_SV=sqrt(Taub_surN_XY_IkenBound_Cmax04_SV(:,5).^2+Taub_surN_XY_IkenBound_Cmax04_SV(:,6).^2);
Cs_Cmax04_SV=Taub_surN_XY_IkenBound_Cmax04_SV(:,3)./(ub_Cmax04_SV.^m.*(1-(Taub_surN_XY_IkenBound_Cmax04_SV(:,3)./(Cmax04.*Taub_surN_XY_IkenBound_Cmax04_SV(:,4))).^(1/m)).^m);

Cs_Cmax04_moy_SV=[];
Cs_Cmax04_min_SV=[];
Cs_Cmax04_max_SV=[];
tmp=[];

for i=1:size(Clim_eff_tmp,1)
    i
    tmp=[Cs_Cmax04_SV,logical((Taub_surN_XY_IkenBound_Cmax04_SV(:,3)./Taub_surN_XY_IkenBound_Cmax04_SV(:,4))>Clim_eff_tmp(i)*Cmax04)];
    tmp(tmp(:,2)==1,:)=[];
    Cs_Cmax04_min_tmp=min(tmp(:,1))
    Cs_Cmax04_max_tmp=max(tmp(:,1))
    Cs_Cmax04_moy_tmp=mean(tmp(:,1))
    
    Cs_Cmax04_min_SV=[Cs_Cmax04_min_SV;Cs_Cmax04_min_tmp];
    Cs_Cmax04_max_SV=[Cs_Cmax04_max_SV;Cs_Cmax04_max_tmp];
    Cs_Cmax04_moy_SV=[Cs_Cmax04_moy_SV;Cs_Cmax04_moy_tmp];
end

%%%~~~Cmax=0.6
ub_Cmax06_SV=sqrt(Taub_surN_XY_IkenBound_Cmax06_SV(:,5).^2+Taub_surN_XY_IkenBound_Cmax06_SV(:,6).^2);
Cs_Cmax06_SV=Taub_surN_XY_IkenBound_Cmax06_SV(:,3)./(ub_Cmax06_SV.^m.*(1-(Taub_surN_XY_IkenBound_Cmax06_SV(:,3)./(Cmax06.*Taub_surN_XY_IkenBound_Cmax06_SV(:,4))).^(1/m)).^m);

Cs_Cmax06_moy_SV=[];
Cs_Cmax06_min_SV=[];
Cs_Cmax06_max_SV=[];
tmp=[];

for i=1:size(Clim_eff_tmp,1)
    i
    tmp=[Cs_Cmax06_SV,logical((Taub_surN_XY_IkenBound_Cmax06_SV(:,3)./Taub_surN_XY_IkenBound_Cmax06_SV(:,4))>Clim_eff_tmp(i)*Cmax06)];
    tmp(tmp(:,2)==1,:)=[];
    Cs_Cmax06_min_tmp=min(tmp(:,1))
    Cs_Cmax06_max_tmp=max(tmp(:,1))
    Cs_Cmax06_moy_tmp=mean(tmp(:,1))
    
    Cs_Cmax06_min_SV=[Cs_Cmax06_min_SV;Cs_Cmax06_min_tmp];
    Cs_Cmax06_max_SV=[Cs_Cmax06_max_SV;Cs_Cmax06_max_tmp];
    Cs_Cmax06_moy_SV=[Cs_Cmax06_moy_SV;Cs_Cmax06_moy_tmp];
end


%%%%~~~~~~~~~~~ON TRACE LES INFOS~~~~~~~~~~~~~~~~~~~~~~~~~
fig4 = figure('renderer','painters','units','centimeters','position',[0 0 17.8 13]);hold on;
% figure(10); hold on;
% l1=plot(Clim_eff_tmp,Cs_Cmax04_min,'g:')
l2=plot(Clim_eff_tmp,Cs_Cmax04_moy_SV,'g-')
l3=plot(Clim_eff_tmp,Cs_Cmax04_max_SV,'g--')
% l4=plot(Clim_eff_tmp,Cs_Cmax06_min,'b:')
l5=plot(Clim_eff_tmp,Cs_Cmax06_moy_SV,'b-')
l6=plot(Clim_eff_tmp,Cs_Cmax06_max_SV,'b--')
hL = legend([l2,l3,l5,l6],{'C_{max}=0.4, C_S moyen','C_{max}=0.4, C_S max','C_{max}=0.6, C_S moyen','C_{max}=0.6, C_S max'})
xlabel('valeur C_{lim}')
ylabel('Valeur C_S')
ax=gca
ax.Box = 'On'
title('Moyenne et maxi du C_S pour deux C_{max} selon C_{lim}')
% xlim([0 2])
grid on 

%% Etape Final : On choisit un Clim_final et on sort Cs pour tous les noeuds qui respectent tb/N < Clim_final * Cmax avec Cmax = 0.4 et Cmax = 0.6
%%%% Attention ici on veut garder les noeuds flottants et leur affecté un Cs petit

Clim_final = 0.8;

%%%% Etape préléminaire : récupérer les infos sur tous les noeuds (même flottant)
XYTaub_Allnodes_SV=[XY_Data_SV(:,X), XY_Data_SV(:,Y), XY_Taub_SV(:,3), XY_Data_SV(:,Neff), XY_Data_SV(:,U), XY_Data_SV(:,V) logical(XY_Data_SV(:,Neff)==0)];

%%%%%%% Pour Cmax = 04

%%% 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont flottants
XYTaub_CsFinal_Cmax04_SV=[XYTaub_Allnodes_SV(:,1:7),logical((XYTaub_Allnodes_SV(:,3)./XYTaub_Allnodes_SV(:,4))>Clim_final*Cmax04)];
XYTaub_CsFinal_Cmax04_SV(XYTaub_CsFinal_Cmax04_SV(:,7)==0 & XYTaub_CsFinal_Cmax04_SV(:,8)==1,:)=[];
XYTaub_CsFinal_Cmax04_SV=XYTaub_CsFinal_Cmax04_SV(:,1:7);


%%% 2- on crée le fichier X Y Cs correspondant à ces noeuds
ubFinal_Cmax04_tmp_SV = sqrt(XYTaub_CsFinal_Cmax04_SV(:,5).^2+XYTaub_CsFinal_Cmax04_SV(:,6).^2);
CsFinal_Cmax04_tmp_SV = XYTaub_CsFinal_Cmax04_SV(:,3)./(ubFinal_Cmax04_tmp_SV.^m.*(1-(XYTaub_CsFinal_Cmax04_SV(:,3)./(Cmax04.*XYTaub_CsFinal_Cmax04_SV(:,4))).^(1/m)).^m);

CsFinal_Cmax04_tmp_SV = [XYTaub_CsFinal_Cmax04_SV(:,1:2),XYTaub_CsFinal_Cmax04_SV(:,7),CsFinal_Cmax04_tmp_SV];

%%% 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique zones posées à écoulement rapide)
CsFinal_Cmax04_tmp_SV(CsFinal_Cmax04_tmp_SV(:,3)==1,4) = 10^-3;
CsFinal_Cmax04_SV = [CsFinal_Cmax04_tmp_SV(:,1:2),CsFinal_Cmax04_tmp_SV(:,4)];

%%% 4- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV' par 'SansVisco' ou autre directement dans dossier de sauvegarde
% save('Cs_Cmax04_Clim08_PROV.dat','CsFinal_Cmax04','-ASCII')

%%%%%%% Pour Cmax = 06

%%% 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont flottants
XYTaub_CsFinal_Cmax06_SV=[XYTaub_Allnodes_SV(:,1:7),logical((XYTaub_Allnodes_SV(:,3)./XYTaub_Allnodes_SV(:,4))>Clim_final*Cmax06)];
XYTaub_CsFinal_Cmax06_SV(XYTaub_CsFinal_Cmax06_SV(:,7)==0 & XYTaub_CsFinal_Cmax06_SV(:,8)==1,:)=[];
XYTaub_CsFinal_Cmax06_SV=XYTaub_CsFinal_Cmax06_SV(:,1:7);


%%% 2- on crée le fichier X Y Cs correspondant à ces noeuds
ubFinal_Cmax06_tmp_SV = sqrt(XYTaub_CsFinal_Cmax06_SV(:,5).^2+XYTaub_CsFinal_Cmax06_SV(:,6).^2);
CsFinal_Cmax06_tmp_SV = XYTaub_CsFinal_Cmax06_SV(:,3)./(ubFinal_Cmax06_tmp_SV.^m.*(1-(XYTaub_CsFinal_Cmax06_SV(:,3)./(Cmax06.*XYTaub_CsFinal_Cmax06_SV(:,4))).^(1/m)).^m);

CsFinal_Cmax06_tmp_SV = [XYTaub_CsFinal_Cmax06_SV(:,1:2),XYTaub_CsFinal_Cmax06_SV(:,7),CsFinal_Cmax06_tmp_SV];

%%% 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique zones posées à écoulement rapide)
CsFinal_Cmax06_tmp_SV(CsFinal_Cmax06_tmp_SV(:,3)==1,4) = 10^-3;
CsFinal_Cmax06_SV = [CsFinal_Cmax06_tmp_SV(:,1:2),CsFinal_Cmax06_tmp_SV(:,4)];

%%% 4- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV' par 'SansVisco' ou autre directement dans dossier de sauvegarde
% save('Cs_Cmax06_Clim08_PROV.dat','CsFinal_Cmax06_SV','-ASCII')

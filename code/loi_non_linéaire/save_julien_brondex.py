# this code have been translate from the matlab code of julien brondex used in Brondex, 2019
# it take the data from the inversion to compute the parameter of
# different friction

import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd
# fixed variable
m = 1 / 3  # Exposant loi Weertman NL ou Schoof
Cmax = 0.4  # Cmax loi Schoof
LineWidth = 1.3
g = 9.81
rho_ice = 910
rho_sea = 1028

# variable depending of your file
v_GM = 0
v_a = 4
v_Neff = 6
v_U = 13
v_V = 14
v_X = 19
v_Y = 20

# choose law used
# ex: law=[0,1] for 0 weertman non linear and 1 for schoof
law = [0, 1]

# load data
file = np.loadtxt('test.dat', delimiter=',')

# define variable from file
alpha = file[:, v_a]
x = file[:, v_X]
y = file[:, v_Y]
u = file[:, v_U]
v = file[:, v_V]
GM = file[:, v_GM]

beta = 10**alpha

# calcul de la pression effective
Neff = file[:, v_Neff]
Neff[GM == -1] = 0

# Weertman linear
# calul du tau_b initiale
taub = beta * np.sqrt(u**2 + v**2)
taub[GM == -1] = 0

if 0 in law:

    # POUR LA LOI DE WEERTMAN NON LINEAIRE

    # Inversion du Beta_wnl

    Beta_wnl = taub / (np.sqrt(u**2 + v**2))**m
    Beta_wnl = {'X': x, 'Y': y, 'Beta_wnl': Beta_wnl}
    Beta_wnl = pd.DataFrame(Beta_wnl, columns=['X', 'Y', 'Beta_wnl'])
    Beta_wnl.to_csv('Beta_wnl.dat', sep='\t', encoding='ascii')


if 1 in law:

    # POUR LA LOI DE SCHOOF

    # Etape Final : On choisit un Clim_final et on sort Cs pour tous les noeuds qui respectent tb/N < Clim_final * Cmax avec Cmax = 0.4 et Cmax = 0.6
    # Attention ici on veut garder les noeuds flottants et leur affecté un Cs
    # petit

    Clim_final = 0.8

    # Pour Cmax = 04
    # 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont
    # flottants
    Nf = (Neff == 0)
    tbN04 = (taub / Neff > Clim_final * Cmax04)
    logical = (Nf == 0) & (tbN04 == 1)

    # XYTaub_CsFinal_Cmax04_SV
    x04 = x[np.logical_not(logical)]
    y04 = y[np.logical_not(logical)]
    u04 = u[np.logical_not(logical)]
    v04 = v[np.logical_not(logical)]
    GM04 = GM[np.logical_not(logical)]
    Neff04 = Neff[np.logical_not(logical)]
    taub04 = taub[np.logical_not(logical)]
    Nf04 = Nf[np.logical_not(logical)]

    # 2- on crée le fichier X Y Cs correspondant à ces noeuds
    ubFinal_Cmax04_tmp_SV = np.sqrt(u04**2 + v04**2)
    CsFinal_Cmax04_SV = taub04 / \
        (ubFinal_Cmax04_tmp_SV**m * (1 - (taub04 / (Cmax04 * Neff04))**(1 / m))**m)

    # 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique
    # zones posées à écoulement rapide)
    CsFinal_Cmax04_SV[Nf04] = 10**-3

    # 4- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV'
    # par 'SansVisco' ou autre directement dans dossier de sauvegarde
    CsFinal_Cmax04_SV = {'X': x04, 'Y': y04,
                         'CsFinal_Cmax04_SV': CsFinal_Cmax04_SV}
    CsFinal_Cmax04_SV = pd.DataFrame(
        CsFinal_Cmax04_SV, columns=[
            'X', 'Y', 'CsFinal_Cmax04_SV'])
    CsFinal_Cmax04_SV.to_csv(
        'Cs_Cmax04_Clim08_PROV.dat',
        sep='\t',
        encoding='ascii')

    # # Pour Cmax = 06

    # # 1- on retire tous les noeuds tq tb/N < Clim_final*Cmax sauf s'ils sont flottants
    tbN06 = (taub / Neff > Clim_final * Cmax06)
    logical = (Nf == 0) & (tbN06 == 1)

    # XYTaub_CsFinal_Cmax06_SV
    x06 = x[np.logical_not(logical)]
    y06 = y[np.logical_not(logical)]
    u06 = u[np.logical_not(logical)]
    v06 = v[np.logical_not(logical)]
    GM06 = GM[np.logical_not(logical)]
    Neff06 = Neff[np.logical_not(logical)]
    taub06 = taub[np.logical_not(logical)]
    Nf06 = Nf[np.logical_not(logical)]

    # 2- on crée le fichier X Y Cs correspondant à ces noeuds
    ubFinal_Cmax06_tmp_SV = np.sqrt(u06**2 + v06**2)
    CsFinal_Cmax06_SV = taub06 / \
        (ubFinal_Cmax06_tmp_SV**m * (1 - (taub06 / (Cmax06 * Neff06))**(1 / m))**m)

    # 3- A tous les noeuds flottants on affecte la valeur Cs = 10^-3 (typique
    # zones posées à écoulement rapide)
    CsFinal_Cmax06_SV[Nf06] = 10**-3

    # # 6- on sauvegarde fichier X Y Cs. Pour plus de sécurité, modifier 'PROV' par 'SansVisco' ou autre directement dans dossier de sauvegarde
    CsFinal_Cmax06_SV = {'X': x06, 'Y': y06,
                         'CsFinal_Cmax06_SV': CsFinal_Cmax06_SV}
    CsFinal_Cmax06_SV = pd.DataFrame(
        CsFinal_Cmax06_SV, columns=[
            'X', 'Y', 'CsFinal_Cmax06_SV'])
    CsFinal_Cmax06_SV.to_csv(
        'Cs_Cmax06_Clim08_PROV.dat',
        sep='\t',
        encoding='ascii')
    # +end_src
